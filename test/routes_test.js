const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})
	

})
